import logging
import os

from sklearn.linear_model import SGDClassifier

from model.classifier import Classifier

log_file = 'logs/out.log'


def setup_logging():
    os.makedirs(os.path.dirname(log_file), exist_ok=True)
    # noinspection PyArgumentList
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler(log_file, mode='a'),
            logging.StreamHandler()
        ]
    )


def main():
    setup_logging()
    classifier = Classifier(SGDClassifier(random_state=42, max_iter=1000, tol=1e-3), labels=['NoSRF', 'SRF'])

    classifier.load_train_set('./data/trainset')
    classifier.fit()

    classifier.load_test_set('./data/testset')
    classifier.evaluate()


if __name__ == "__main__":
    main()
