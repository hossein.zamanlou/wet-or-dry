import logging.config
import os

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

from model.preprocessor import Preprocessor


class Classifier:
    logger = logging.getLogger(__name__)

    labels = []
    train_set = []
    test_set = {}
    clf = 0

    def __init__(self, classifier, labels):
        self.logger.info('Initializing classifier for labels %s...' % labels)
        self.clf = classifier
        self.labels = labels

    def load_train_set(self, path):
        for l in self.labels:
            self.train_set = self.train_set + [(Preprocessor(entry.path).execute(), l) for entry in
                                               os.scandir("{}/{}".format(path, l))]

    def load_test_set(self, path):
        self.test_set = [Preprocessor(entry.path).execute() for entry in os.scandir(path)]

    def fit(self):
        x, y = zip(*self.train_set)

        # Split into labeled train and test set
        X_train, X_test, y_train, y_test = train_test_split(
            x, y,
            test_size=0.2,
            shuffle=True,
            random_state=42
        )

        # Fit the model
        self.clf.fit(X_train, y_train)

        # Do prediction on unseen data
        y_pred = self.clf.predict(X_test)

        # Print out metrics
        score = self.clf.score(X_test, y_test)
        print('Percentage correct: %.2f%%' % (100 * score))
        print(classification_report(y_test, y_pred, target_names=self.labels))

        # cmx = confusion_matrix(y_test, y_pred)
        # print(cmx)

    def evaluate(self):
        y_pred = self.clf.predict(self.test_set)
