import logging.config

import numpy as np
from PIL import Image
from skimage.color import rgb2gray, rgba2rgb
from skimage.feature import hog


class Preprocessor:
    logger = logging.getLogger(__name__)

    def __init__(self, image_path):
        self.img = np.array(Image.open(image_path))

    def execute(self):
        self.to_grayscale()
        self.hog()
        self.reshape()

        return self.img

    def to_grayscale(self):
        self.img = rgb2gray(rgba2rgb(self.img))

    def hog(self):
        _, self.img = hog(self.img, orientations=8, pixels_per_cell=(16, 16),
                          cells_per_block=(1, 1), visualize=True)

    def reshape(self):
        self.img = self.img.reshape(self.img.shape[0] * self.img.shape[1])
